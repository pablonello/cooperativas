<?php


class Cooperativa extends MY_Model {

    public $table = "base";
    public $table_id = "id";

    public function __construct() {
        parent::__construct();
    }

    public function buscarCooperativa($idCooperativa) {
        $this->db->select('*');
        $this->db->from('base');
        $this->db->where('id', $idCooperativa);
        $consulta = $this->db->get();

        if ($consulta->num_rows() > 0) {
            return $consulta->result();
        } else {
            return false;
        }
        
    }

}
