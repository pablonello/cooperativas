<?php

class App extends MY_Controller {

    public function __construct() {
        parent::__construct();

        /*
         * libreria para cargar el body del template en etodas las web html
         */
        $this->load->library("Form_validation");
        $this->load->database();


        $this->load->helper('form');
    }

    public function login() {
        $view["body"] = $this->load->view("app/login", NULL, TRUE);
        $this->parser->parse("template/body_login", $view);
    }

//*******************************************************************************************
    public function ingresar_login() {

        if ($this->input->post()) {
            $username = $this->input->post('login_string');
            $passwd = $this->input->post('login_pass');

            $this->load->model('Usuario');

            $pswencri = $this->Usuario->encryption($passwd);

            $usuario = $this->Usuario->usuario_por_nombre_contrasena($username, $pswencri);

            if ($usuario) {

                $usuario_data = array(
                    'id' => $usuario->idusuarios,
                    'nombre' => $usuario->usuario,
                    'psw' => $usuario->psw,
                    'logueado' => TRUE
                );
                $this->session->set_userdata($usuario_data);
                    redirect('Admin');
            } else {
                $this->session->set_flashdata('Login_fallido', 'Usuario o contraseña incorrectos');
                redirect('App/login');
            }
        } else {
            $this->session->set_flashdata('Login_fallido', 'Usuario o contraseña incorrectos');
            redirect('App/login');
        }
    }


    public function cerrar_sesion() {
        $usuario_data = array(
            'logueado' => FALSE
        );
        $this->session->set_userdata($usuario_data);
        $this->session->sess_destroy();
        redirect('App/login');
    }

    public function profile() {

//        $this->load->helper('Breadcrumb_helper');

        $data['usuario'] = $this->Usuario->find($this->session->userdata('id'));

        $view["body"] = $this->load->view("app/profile", $data, TRUE);
        $view["title"] = 'Perfil';
//        $view['breadcrumb'] = breadcrumb_admin("profile");
        $this->parser->parse("template/body", $view);
    }

    

}
