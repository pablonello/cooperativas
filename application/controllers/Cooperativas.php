<?php

class Cooperativas extends MY_Controller {

    public function __construct() {
        parent::__construct();

        /*
         * libreria para cargar el body del template en etodas las web html
         */
        $this->load->library('Form_validation');
        $this->load->helper('form');
    }

    public function index() {
        if ($this->session->userdata('logueado')) {
            redirect("Cooperativas/list_cooperativas");
        } else {
            show_404();
        }
    }

    /*
     * Listar Usuarios
     */

    public function list_cooperativasFiltro() {
        if ($this->session->userdata('logueado')) {

            $crud = new grocery_CRUD();                 //creo un nuevo objeto de la libreria CRUD
            $crud->set_table('base');              //selecciono la tabla
            $crud->set_subject('Cooperativas');            //pongo el titulo al boton para agregar un registro nuevo
            $crud->columns('autorizacionPFuncionar', 'planesNacion', 'registro', 'cierre', 'denominacionUsual', 'denominacionRegistro', 'domicilioLegal', 'telefono', 'mail', 'sede', 'cuit', 'ejerciciosAdeudados', 'mora', 'retiroAutorizacionPFuncionar', 'emplazamiento', 'ultimaAgo', 'expediente', 'ultBalance', 'observaciones', 'contitucion', 'canceladas');    //pongo las columnas a mostrar

            $crud->unset_jquery();
            $crud->unset_clone();
            $crud->unset_read();
            $crud->unset_edit();
            $crud->unset_add();


            $output = $crud->render(); //renderiso el tema en la pagina web


            $view["grocery_crud"] = json_encode($output);
            $view["title"] = "Lista de Cooperativas";
            $this->parser->parse("template/body", $view);
        } else {
            show_404();
        }
    }

    public function list_cooperativas() {
        if ($this->session->userdata('logueado')) {

//       
            $crud = new grocery_CRUD();                 //creo un nuevo objeto de la libreria CRUD
//        $crud->set_theme('datatables');             //selecciono el tema
            $crud->set_table('base');              //selecciono la tabla
            $crud->set_subject('Cooperativas');            //pongo el titulo al boton para agregar un registro nuevo
            $crud->columns('registro', 'denominacionUsual', 'mora');    //pongo las columnas a mostrar
            //titulos columnas
            $crud->display_as('registro', 'registro');
            $crud->display_as('denominacionUsual', 'Denominacion Usual');
            $crud->display_as('mora', 'Mora');

            $crud->unset_jquery();
            $crud->unset_clone();

//            $crud->add_action('','https://4topiso.com/cooperativas/assets/img/PDF.png', 'Cooperativas/imprimirPDF');
            $crud->add_action('PDF1', 'https://4topiso.com/cooperativas/assets/img/PDF.png', 'Cooperativas/imprimirPDF');

            $output = $crud->render(); //renderiso el tema en la pagina web

            $view["grocery_crud"] = json_encode($output);
            $view["title"] = "Lista de Cooperativas";
            $this->parser->parse("template/body", $view);
        } else {
            show_404();
        }
//      
    }

    public function imprimirPDF($idCooperativa) {
        if ($this->session->userdata('logueado')) {
            $buscarCooperativas = $this->Cooperativa->buscarCooperativa($idCooperativa);
//
//             print '<pre>';
//            print_r($buscarCooperativas[0]);
//            print '</pre>';
//            die();
            $data['autorizacionPFuncionar'] = $buscarCooperativas[0]->autorizacionPFuncionar;
            $data['planesNacion'] = $buscarCooperativas[0]->planesNacion;
            $data['registro'] = $buscarCooperativas[0]->registro;
            $data['cierre'] = $buscarCooperativas[0]->cierre;
            $data['denominacionUsual'] = $buscarCooperativas[0]->denominacionUsual;
            $data['denominacionRegistro'] = $buscarCooperativas[0]->denominacionRegistro;
            $data['domicilioLegal'] = $buscarCooperativas[0]->domicilioLegal;
            $data['telefono'] = $buscarCooperativas[0]->telefono;
            $data['mail'] = $buscarCooperativas[0]->mail;
            $data['sede'] = $buscarCooperativas[0]->sede;
            $data['cuit'] = $buscarCooperativas[0]->cuit;
            $data['ejerciciosAdeudados'] = $buscarCooperativas[0]->ejerciciosAdeudados;
            $data['mora'] = $buscarCooperativas[0]->mora;
            $data['retiroAutorizacionPFuncionar'] = $buscarCooperativas[0]->retiroAutorizacionPFuncionar;
            $data['emplazamiento'] = $buscarCooperativas[0]->emplazamiento;

            if (empty($buscarCooperativas[0]->ultimaAgo)) {
                $data['ultimaAgo'] = '';
            } else {
                $ultimaAgo = $buscarCooperativas[0]->ultimaAgo;
                $ultimaAgoAux = date("d/m/Y", strtotime($ultimaAgo));
                $data['ultimaAgo'] = $ultimaAgoAux;
            }

            $data['expediente'] = $buscarCooperativas[0]->expediente;
            if (empty($buscarCooperativas[0]->ultBalance)) {
                $data['ultBalance'] = '';
            } else {
                $ultBalance = $buscarCooperativas[0]->ultBalance;
                $ultBalanceAux = date("d/m/Y", strtotime($ultBalance));
                $data['ultBalance'] = $ultBalanceAux;
            }
            $data['observaciones'] = $buscarCooperativas[0]->observaciones;
            $data['contitucion'] = $buscarCooperativas[0]->contitucion;
            $data['canceladas'] = $buscarCooperativas[0]->canceladas;


            $this->load->view('cooperativas/printPDF', $data);

            // Get output html
            $html = $this->output->get_output();

            // Load pdf library
            $this->load->library('pdf');

            // Load HTML content
            $this->dompdf->loadHtml($html);

            // (Optional) Setup the paper size and orientation
            $this->dompdf->setPaper('A4', 'portrait');

            // Render the HTML as PDF
            $this->dompdf->render();

            // Output the generated PDF (1 = download and 0 = preview)
            $this->dompdf->stream("Cooperativa.pdf", array("Attachment" => 1));

            //Guardalo en una variable
            $output = $this->dompdf->output();
        } else {
            $this->session->set_flashdata('Login_fallido', 'Se cerro la sesion');
            redirect('App/login');
        }
    }

}
