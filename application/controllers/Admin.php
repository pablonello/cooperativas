<?php

class Admin extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        if ($this->session->userdata('logueado')) {
            $data['nombre'] = $this->session->userdata('nombre');
            $view["body"] = $this->load->view("admin/index", $data, TRUE);
            $view["title"] = "Bienvenido/a ";
            $this->parser->parse("template/body", $view);
        } else {
            show_404();
        }
    }

}