
<h2 style="text-align: center"><strong>DATOS COOPERATIVA</strong></h2>

<div class="card-header" style="text-align: center">
    <strong>Fecha de Consulta: </strong> 
    <?php
    $hoy = date('d-m-Y');
    
    echo $hoy;
    ?>
    <br>
    <strong>Nombre de la Cooperativa: </strong> <?php echo $denominacionUsual ?>
    <br>
    <strong>Domicilio Legal: </strong> <?php echo $domicilioLegal ?>

</div>
<br>
<table width="600px" cellpadding="3px" cellspacing="3px" border="1" style=" margin: auto;font-size: 10pt;">

    <tr>
        <td>Autorizacion para Funcionar: </td>
        <td><?php echo $autorizacionPFuncionar; ?></td>
    </tr>
    <tr>
        <td>Planes Nación: </td>
        <td><?php echo $planesNacion; ?></td>
    </tr>
    <tr>
        <td>Registros: </td>
        <td><?php echo $registro; ?></td>
    </tr>
    <tr>
        <td>Cierre: </td>
        <td><?php echo $cierre; ?></td>
    </tr>
    <tr>
        <td>Denominación Usual: </td>
        <td><?php echo $denominacionUsual; ?></td>
    </tr>
    <tr>
        <td>Denominación Registro: </td>
        <td><?php echo $denominacionRegistro; ?></td>
    </tr>
    <tr>
        <td>Domicilio Legal: </td>
        <td><?php echo $domicilioLegal; ?></td>
    </tr>
    <tr>
        <td>Telefono: </td>
        <td><?php echo $telefono; ?></td>
    </tr>
    <tr>
        <td>Mail: </td>
        <td><?php echo $mail; ?></td>
    </tr>
    <tr>
        <td>Sede: </td>
        <td><?php echo $sede; ?></td>
    </tr>
    <tr>
        <td>Cuit: </td>
        <td><?php echo $cuit; ?></td>
    </tr>
    <tr>
        <td>Ejercicios Adeudados</td>
        <td><?php echo $ejerciciosAdeudados; ?></td>
    </tr>
    <tr>
        <td>Mora: </td>
        <td><?php echo $mora; ?></td>
    </tr>
    <tr>
        <td>Retiro Autorizacion para Funcionar</td>
        <td><?php echo $retiroAutorizacionPFuncionar; ?></td>
    </tr>
    <tr>
        <td>Emplazamiento</td>
        <td><?php echo $emplazamiento; ?></td>
    </tr>
    <tr>
        <td>Ultima A.G.O.: </td>
        <td><?php
//            $ultimaAgoAux = date("d/m/Y", strtotime($ultimaAgo));
            echo $ultimaAgo;
            ?></td>
    </tr>
    <tr>
        <td>Expediente</td>
        <td><?php echo $expediente; ?></td>
    </tr>
    <tr>
        <td>Ultimo Balance</td>
        <td><?php
//            $ultBalanceAux = date("d/m/Y", strtotime($ultBalance));
            echo $ultBalance;
            ?></td>
    </tr>
    <tr>
        <td>Observaciones</td>
        <td><?php echo $observaciones; ?></td>
    </tr>
    <tr>
        <td>Constitución</td>
        <td><?php echo $contitucion; ?></td>
    </tr>
    <tr>
        <td>Canceladas</td>
        <td><?php echo $canceladas; ?></td>
    </tr>

</table>
