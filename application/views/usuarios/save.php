<?php echo form_open('', 'class"my_form" enctype="multipart/form-data"'); ?>


<div class="col-lg-6" >
    <div class="card">

        <div class="card-body">
            <div class="form-group">
                <?php
                echo form_label('Usuario', 'usuario');
                ?>
                <?php
                $text_input = array(
                    'name' => 'usuario',
                    'id' => 'usuario',
                    'value' => $usuario,
                    'class' => 'form-control input-lg',
                );

                echo form_input($text_input);
                ?>
                <?php
                echo form_error('usuario', '<div class="text-error">', '</div>')
                ?>
            </div> 
            <div class="form-group">
                <?php
                echo form_label('Password', 'psw');
                ?>
                <?php
                $text_input = array(
                    'name' => 'psw',
                    'id' => 'psw',
                    'value' => $psw,
                    'type' => "password",
                    'class' => 'form-control input-lg',
                );

                echo form_input($text_input);
                ?>
                <?php
                echo form_error('psw', '<div class="text-error">', '</div>')
                ?>
                <br>
            </div>

            <div class="form-group">
                <?php
                //armo el arreglo con las opciones de los tipos de usuario
                $tipo = array(
                    'administrador' => 'administrador',
                    'usuarioAdministrador' => 'usuarioAdministrador',
                    'usuario' => 'usuario'
                );

                //lista desplegable
                echo form_label('Tipo', 'tipo');
                echo form_dropdown('tipo', $tipo, 'administrador', 'class="form-control input-lg"');
                ?>
            </div>
            <div class="form-group">
                <?php
                //armo el arreglo con las opciones de los tipos de usuario
                $nivel = array(
                    '0' => '0',
                    '5' => '5',
                    '10' => '10',
                );
                //lista desplegable
                echo form_label('Nivel usuario', 'nivel');
                echo form_dropdown('nivel', $nivel, '1', 'class="form-control input-lg"');
                ?>
            </div>
            <div class="form-group">
                <?php
                echo form_label('Correo', 'correo');
                ?>
                <?php
                $text_input = array(
                    'name' => 'correo',
                    'id' => 'correo',
                    'value' => $correo,
                    'class' => 'form-control input-lg',
                );

                echo form_input($text_input);
                ?>
                <?php
                echo form_error('correo', '<div class="text-error">', '</div>')
                ?>
                <br>
            </div>

        </div>
    </div>
</div>
<br>

<?php echo form_submit('mysbumit', 'Actualizar', 'class="btn btn-primary"') ?>
<a class="btn btn-primary" 
   href="<?php echo base_url() . 'Usuarios' ?>"> Volver
</a>

<?php echo form_close() ?>
                          