<div class="login-box">
    <div class="login-logo">
        <a href="../../index2.html"><b><?php echo APP_NAME ?></b></a>
    </div>
    <!-- /.login-logo -->
    <?php if ($error = $this->session->flashdata('Login_fallido')) { ?>
        <div class="row">
            <div >
                <div class="alert alert-dismissible alert-danger">
                    <?php echo $error ?>
                </div>
            </div>
        </div>
    <?php } ?>

    <div class="card">
        <div class="card-body login-card-body">

            <p class="login-box-msg">Iniciar Sesion</p>

            <form action="<?php echo base_url() ?>App/ingresar_login" method="post">
                <div class="input-group mb-3">
                    <input name="login_string" class="form-control" placeholder="Nombre de Usuario o Correo" type="text">

                </div>
                <div class="input-group mb-3">
                    <input name="login_pass" class="form-control" placeholder="Password" type="password">

                </div>
                <div class="col-4">
                    <button type="submit" class="btn btn-primary btn-block">Ingresar</button>
                </div>

            </form>


        </div>
        <!-- /.login-card-body -->
    </div>
</div>


