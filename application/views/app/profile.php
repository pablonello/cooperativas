<div class="row">

    <div class="col-lg-6" style="float:left;">
        <div class="card">
            <div class="card-header">
                <h4><i class="fa fa-user"></i> Datos de usuario</h4>
            </div>
            <div class="card-body">
                
                <div class="form-group">
                    <?php
                    echo form_label('Usuario', 'usuario');
                    ?>
                    <?php
                    $text_input = array(
                        'readonly' => 'readonly',
                        'value' => $this->session->userdata('nombre'),
                        'class' => 'form-control input-lg',
                    );
                    echo form_input($text_input);
                    ?>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Tipo', 'tipo');
                    ?>
                    <?php
                    $text_input = array(
                        'readonly' => 'readonly',
                        'value' => $this->session->userdata('tipo'),
                        'class' => 'form-control input-lg',
                    );
                    echo form_input($text_input);
                    ?>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Correo', 'correo');
                    ?>
                    <?php
                    $text_input = array(
                        'readonly' => 'readonly',
                        'value' => $this->session->userdata('correo'),
                        'class' => 'form-control input-lg',
                    );
                    echo form_input($text_input);
                    ?>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Nivel', 'nivel');
                    ?>
                    <?php
                    $text_input = array(
                        'readonly' => 'readonly',
                        'value' => $this->session->userdata('nivel'),
                        'class' => 'form-control input-lg',
                    );
                    echo form_input($text_input);
                    ?>
                </div>
               
            </div>


        </div>
    </div>
    <div style="float:left;">
        <div class="card">
            <div class="card-header">
                <h4><i class="fa fa-usb"></i> Información</h4>
            </div>
            <div class="card-body" >
                <div class="form-group">
                    <p>Datos del usuario actual en <strong>sesion</strong></p>
                    <p>para poder modificar estos datos, ponganse en contacto con <strong>4topiso</strong></p>
                </div>
                
            </div>


        </div>
    </div>

</div>