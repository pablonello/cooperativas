<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;
class Pdf {

    public function __construct() {

        // include autoloader
        require_once dirname(__FILE__).'/dompdf/autoload.inc.php';

        //cargo las opciones asi puedo cargar imagenes en los PDF
        $options = new Options();
        $options->set('isRemoteEnabled', TRUE);
        // instantiate and use the dompdf class
        $pdf = new Dompdf($options);

        $CI = & get_instance();
        $CI->dompdf = $pdf;
    }

}

?>